#!/bin/bash
#
#	Copyright (c) 2016-2020, Carlos Donizete Froes <coringao@riseup.net>
#	Use of this script is governed by a BSD 2-clause license
#	that can be found in the LICENSE file.
#	Source code and contact info at https://gitlab.com/coringao/runescape
#
#	Game Terms and Conditions: Copyright (c) 1999-2020, Jagex Ltd
#	Use of this website is subject to our Terms & Conditions[1],
#	Privacy Policy[2] and Cookie Policy[3].
#
#	[1] https://www.jagex.com/terms
#	[2] https://www.jagex.com/terms/privacy
#	[3] https://www.jagex.com/terms/cookies
#
# Script Name:	runescape.sh
# Update Date:	April/2020
# Edited version: '0.8'
#
# Download Mac Client - Old School Runescape
LINK="https://oldschool.runescape.com/downloads/OldSchool.dmg"
HASH="50660098fce6969464d3ad86aa74d2c6a757949b65fe85ee9487962ff6935bfc2d202496a9687a7f0e04c8b6336dfa1e858f0c3781e24058fb3b3d32c33a9201"

# Hidden directory runescape
GAME="$HOME/.local/share/runescape"

# Feature to show message via kdialog or zenity
showmessage() {
  if [ "$(command -v zenity)" ]; then
    case "$1" in
      --info) zenity --info --title="Old School Runescape Launcher" --width="500" \
              --text="$2" ;;
      --warning) zenity --question --title="Unexpected launcher downloaded" --width="500" \
              --window-icon="warning" --text="$2" ;;
    esac
  elif [ "$(command -v kdialog)" ]; then
    case "$1" in
      --info) kdialog --msgbox "$2" --title="Old School Runescape Launcher" ;;
      --warning) kdialog --yesno "$2" --title="Unexpected launcher downloaded" ;;
    esac
  fi
}

# Verification of the downloaded file against a good hash (Thanks Stephen Kitt)
checkdl() {
	if sha512sum -c - <<<"$HASH OldSchool.dmg"; then
		return 0
	fi

	# Check against the latest version of the shell script
	UPDHASH=$(wget -O- https://gitlab.com/coringao/runescape/-/raw/master/src/runescape.sh 2>/dev/null | awk -F= '/^HASH=/ { print $2 }')
	if [ -n "$UPDHASH" ] && sha512sum -c - <<<"$UPDHASH OldSchool.dmg"; then
		return 0
	fi
	if LANG=C `showmessage --warning "The RuneScape launcher doesn’t correspond to the file expected by this script. \n\nThis could mean a perfectly innocent change, or it could be because the download has been replaced by a malicious file. \n\nDo you want to use the downloaded launcher anyway?" 2>/dev/null`; then
	        return 0
	fi
	return 1
}

if [ ! -f "${GAME}/jagexappletviewer.jar" ]; then
	# Temporary directory will be created and after installation
	# this directory will be deleted
	TEMP="$(mktemp -d /tmp/runescape.XXXXX)"
	trap "rm -rf ${TEMP}" EXIT

	cd $TEMP

	# Download the file in the temporary directory
        LANG=C wget -c $LINK 2>/dev/null
	if ! checkdl; then exit 1; fi

        # Uncompressing the file in the temporary directory
        7z e OldSchool.dmg > /dev/null
        mkdir -p $GAME
        mv jagexappletviewer.jar $GAME
fi

# Preparing sandbox environment and running the game
        LANG=C `showmessage --info "This software is a simple script that downloads and runs the Old School Runescape client file on unofficial GNU/Linux distributions. \n\nThe game works on several different platforms (supported or not by Jagex) using Java installed on the system, avoiding common risks encountered by players using a web browser. \n\nAfter downloading the official client file, the script will prepare the sandbox environment located in '$HOME/.local/share/runescape' and run the game." 2>/dev/null`
	java -Duser.home="$GAME" --add-opens java.base/java.lang=ALL-UNNAMED \
	-Xmx512m -Xms512m -Djava.class.path="$GAME/jagexappletviewer.jar" \
	-Dcom.jagex.config=http://oldschool.runescape.com/jav_config.ws \
	jagexappletviewer "$GAME" > /dev/null 2>&1

exit 0
