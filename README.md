Old School Runescape for Unofficial GNU/Linux
=============================================

[Old School Runescape](https://oldschool.runescape.com) is a massively
multiplayer online role-playing game created by [Jagex Ltd](https://jagex.com).

Explore in an open and mysterious world, with devastated landscapes and
sinister powers.

Having chosen an adventurer, players are free to find their role within it:
to live by the sword and spells facing hundreds of enemies, to further
the storyline in quests, or to train in any of a number of skills.

**Game executable script**
--------------------------

This software is a simple script that downloads and runs
the Old School Runescape client file on unofficial GNU/Linux distributions.

The game works on several different platforms (supported or not by Jagex)
using Java installed on the system, avoiding common risks encountered
by players using a web browser.

After downloading the official client file, the script will prepare the sandbox
environment located in **$HOME/.local/share/runescape** and run the game.

**Dependencies**
----------------

To use the script, you will need to have the following dependencies
installed: **OpenJDK**, **7Zip**, **Wget** and **Zenity**.

**Installation dependency: Debian/Ubuntu/GNU/Linux**
----------------------------------------------------

    # apt install default-jre make p7zip-full wget zenity

When all these dependencies have installed, simply run the script.

Next you need to compile this release.
    
**Make Install and Make Uninstall**
-----------------------------------

Install:

    # make install

Uninstall:
    
    # make uninstall

License
=======

> Game Terms and Conditions: Use of this website is subject
> to our Terms & Conditions[1], Privacy Policy[2] and Cookie Policy[3].
> * [1] https://www.jagex.com/terms
> * [2] https://www.jagex.com/terms/privacy
> * [3] https://www.jagex.com/terms/cookies
>
> Runescape.sh file: Use of this script is governed by a BSD 2-clause license
> that can be found in the LICENSE file. We encourage new contributors
> to distribute files under this license. Source code and contact info at
> https://gitlab.com/coringao/runescape.

* Copyright (c) 1999-2020, Jagex Ltd.
* Copyright (c) 2016-2020, Carlos Donizete Froes [a.k.a coringao]
